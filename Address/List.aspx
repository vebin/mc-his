﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Address_List, App_Web_rnejanru" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            通讯录
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">通讯录</li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
          
            <MojoCube:Nav id="Nav" runat="server" />
    
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    <asp:HyperLink ID="hlEditFolder" runat="server" Visible="false"><i class="fa fa-edit"></i></asp:HyperLink>
                    <asp:LinkButton ID="lnbDeleteFolder" runat="server" Visible="false" onclick="lnbDeleteFolder_Click" OnClientClick="{return confirm('该操作将删除分类下面的所有通讯录，确定删除吗？');}"><i class="fa fa-trash-o"></i></asp:LinkButton>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <asp:LinkButton ID="lnbSelectAll" runat="server" CssClass="btn btn-default btn-sm checkbox-toggle" onclick="lnbSelectAll_Click" ToolTip="全选/反选"><i class="fa fa-square-o"></i></asp:LinkButton>
                    <div class="btn-group">
                        <asp:LinkButton ID="lnbDelete" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbDelete_Click" OnClientClick="{return confirm('确定删除吗？');}" ToolTip="删除文件"><i class="fa fa-trash-o"></i></asp:LinkButton>
                        <asp:LinkButton ID="lnbStar" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbStar_Click" ToolTip="标为星标"><i class="fa fa-star"></i></asp:LinkButton>
                    </div>
                    <asp:LinkButton ID="lnbRefresh" runat="server" CssClass="btn btn-default btn-sm" onclick="lnbRefresh_Click" ToolTip="刷新"><i class="fa fa-refresh"></i></asp:LinkButton>
                  </div>
                  <div class="table-responsive mailbox-messages">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover table-striped" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Address") %>'></asp:Label>
                                    <asp:CheckBox ID="cbFolder" runat="server" Checked='<%# Bind("IsFolder") %>' />
                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("ImagePath") %>'></asp:Label>
                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("CreateUser") %>'></asp:Label>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                    <asp:CheckBox ID="cbStar" runat="server" Checked='<%# Bind("IsStar") %>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="选择">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="gvStar" runat="server" ToolTip="标记星标" CommandName="_star"><i class="fa fa-star-o text-yellow"></i></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-star" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="类型" SortExpression="TypeID">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("TypeID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("ImagePath") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="联系人">
                                <ItemTemplate>
                                    <asp:Label ID="lblContact" runat="server" Text='<%# Bind("Contact") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-subject" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="手机号码">
                                <ItemTemplate>
                                    <asp:Label ID="lblPhone1" runat="server" Text='<%# Bind("Phone1") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-date" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="性别">
                                <ItemTemplate>
                                    <asp:Label ID="lblSex" runat="server" Text='<%# Bind("Sex") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="分类">
                                <ItemTemplate>
                                    <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("ParentName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="mailbox-attachment" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="StatusID">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvEdit" runat="server" ToolTip="修改"><span class="label label-primary"><i class="fa fa-edit"></i> 修改</span></asp:HyperLink>
                                    <asp:HyperLink ID="gvView" runat="server" ToolTip="查看"><span class="label label-primary"><i class="fa fa-search"></i> 查看</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <div id="EmptyDiv" runat="server" visible="false" style="height:60px; text-align:center; padding:10px; overflow:auto; border-top:solid 1px #F4F4F4;">暂无信息</div>
                    
                  </div>
                </div>
                <div class="box-footer no-padding" style="margin-top:-20px;">
                  <div class="mailbox-controls">
                  
                    <div id="pager" style="background:#fff; border:0px; margin-top:0px; padding:2px;">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>

      </div>
      
</asp:Content>


