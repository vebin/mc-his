﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_Check, App_Web_3nnyqhi4" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlStep");
            document.getElementById("ctl00_cphMain_hlReceiver").href = "Receiver.aspx?stepId=" + ddl.value;
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            审批编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">审批编辑</li>
          </ol>
        </section>

        <section class="content">
        
          <div id="AlertDiv" runat="server"></div>

          <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">

                <div class="box-header with-border">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  </h3>
                </div>

                <ul class="nav nav-tabs">
                  <li class="active"><a href="#info" data-toggle="tab">基本信息</a></li>
                  <li><a href="#file" data-toggle="tab">附件管理</a></li>
                </ul>
                <div class="tab-content">
                  
                  <div class="active tab-pane" id="info">
                  
                    <div class="box-body">

                        <div class="form-group">
                            <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                        </div>
                        
                        <div class="form-group">
                            <asp:HyperLink ID="hlSignature" runat="server" CssClass="btn btn-success fancybox fancybox.iframe" ToolTip="签章" Width="100px">签章</asp:HyperLink>
                            <span id="Signature"></span>
                            <asp:TextBox ID="txtID" runat="server" style="display:none"></asp:TextBox>
                        </div>
                    
                        <div class="form-group">
                            <label><asp:Label ID="Label2" runat="server" Text="审批意见"></asp:Label></label>
                            <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        </div>

                        <div class="row">
                        
                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label3" runat="server" Text="标题"></asp:Label></label>
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                            </div>
                    
                            <div class="col-md-6 form-group">
                                <label><asp:Label ID="Label5" runat="server" Text="到期"></asp:Label></label>
                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                            </div>
                            
                            <div id="StepDiv" runat="server">

                                <div class="col-md-6 form-group">
                                    <label><asp:Label ID="Label4" runat="server" Text="下一步"></asp:Label></label>
                                    <asp:DropDownList ID="ddlStep" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                                </div>
                            
                                <div class="col-md-6 form-group">
                                    <label><asp:Label ID="Label6" runat="server" Text="紧急程度"></asp:Label></label>
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                                </div>

                            </div>

                        </div>

                        <div id="UserDiv" runat="server" class="form-group" style="position:relative;">
                            <label><asp:Label ID="Label1" runat="server" Text="下一步审批人"></asp:Label></label>
                            <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                            <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                            <div style="position:absolute; top:31px; right:5px;">
                                <asp:HyperLink ID="hlReceiver" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 审批人</span></asp:HyperLink>
                            </div>
                        </div>
                    
                    </div>
                    
                  </div>

                  <div class="tab-pane" id="file">

                    <div class="box-body">

                      <div class="row">
                          <iframe id="ifUpload" runat="server" style="width:100%;" frameborder="0" scrolling="auto" class="iframe-css"></iframe>
                      </div>

                    </div>
            
                  </div>
                  
                  <div class="box-footer">
                      <asp:Button ID="btnAgree" runat="server" Text="同意" CssClass="btn btn-primary" onclick="btnAgree_Click" OnClientClick="{return confirm('确定同意吗？');}"></asp:Button>
                      <asp:Button ID="btnDisagree" runat="server" Text="不同意" CssClass="btn btn-danger" onclick="btnDisagree_Click" OnClientClick="{return confirm('确定不同意吗？');}"></asp:Button>
                      <asp:HyperLink ID="hlReturn" runat="server" NavigateUrl="#ReturnDiv" CssClass="fancybox" ToolTip="退回"><span class="btn btn-warning">退回</span></asp:HyperLink>
                      <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </section>

      </div>
      
      <div id="ReturnDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="form-group">
                <label><asp:Label ID="Label7" runat="server" Text="退回的步骤"></asp:Label></label>
                <asp:DropDownList ID="ddlReturn" runat="server" CssClass="form-control select2" Enabled="false"></asp:DropDownList>
              </div>
                   
              <div class="pull-right">
                  <asp:LinkButton ID="lnbReturn" runat="server" CssClass="btn btn-primary" onclick="lnbReturn_Click" OnClientClick="{return confirm('确定退回吗？');}">退回</asp:LinkButton>
              </div>

          </div>

      </div>
    
</asp:Content>