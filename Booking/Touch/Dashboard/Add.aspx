﻿<%@ page language="C#" autoeventwireup="true" inherits="Booking_Touch_Dashboard_Add, App_Web_0ed0zj05" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>

<body class="body">
    
    <header>

        <div class="topbar">
            <a href="Default.aspx" class="back_btn"><i class="icon iconfont">&#xf0115;</i></a>
            <a href="javascript:;" class="top_home"></a>
            <h1 class="page_title"><asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>

    </header>

    <form id="form1" runat="server">
    
        <table class="edittb">
            <tr>
                <td class="editname">科室</td>
                <td>
                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="ddl" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="editname">医生</td>
                <td>
                    <asp:DropDownList ID="ddlUser" runat="server" CssClass="ddl">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="editname">预约？天后</td>
                <td>
                    <asp:TextBox ID="txtDays" runat="server" CssClass="edittxt" autocomplete="off" placeholder="例如明天填1，后天填2..."></asp:TextBox>
                </td>
            </tr>
        </table>
        
        <div class="btnDiv">
            <asp:Button ID="btnSave" runat="server" Text="提交" CssClass="btnDel" OnClick="btnSave_Click" style="background:#F96410" />
        </div>

        <div id="attention-dialog" class="attention-layout"></div>

    </form>
 
</body>

</html>
