﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Customer_Edit, App_Web_mz0zwgfz" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            患者管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">患者管理</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label29" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label30" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="患者名称"></asp:Label></label>
                    <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label3" runat="server" Text="姓名简拼"></asp:Label></label>
                    <asp:TextBox ID="txtShortName" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="患者性别"></asp:Label></label>
                    <asp:DropDownList ID="ddlCustomerLevel" runat="server" CssClass="form-control select2">
                        <asp:ListItem Text="男" Value="男" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="女" Value="女"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="患者年龄"></asp:Label></label>
                    <asp:TextBox ID="txtCustomerType" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label7" runat="server" Text="出生年月"></asp:Label></label>
                    <asp:TextBox ID="txtTags" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label9" runat="server" Text="出生地"></asp:Label></label>
                    <asp:TextBox ID="txtCustomerSource" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label4" runat="server" Text="患者民族"></asp:Label></label>
                    <asp:TextBox ID="txtCustomerProperty" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label8" runat="server" Text="患者职业"></asp:Label></label>
                    <asp:TextBox ID="txtCustomerState" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label10" runat="server" Text="婚姻状况"></asp:Label></label>
                    <asp:TextBox ID="txtIndustry" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label11" runat="server" Text="公司性质"></asp:Label></label>
                    <asp:TextBox ID="txtNature" runat="server" CssClass="form-control" placeholder="如：国有企业、民营企业..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label12" runat="server" Text="身份证号"></asp:Label></label>
                    <asp:TextBox ID="txtEmployeNum" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label13" runat="server" Text="医保卡号"></asp:Label></label>
                    <asp:TextBox ID="txtLicense" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label14" runat="server" Text="手机号码"></asp:Label></label>
                    <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="患者备注"></asp:Label></label>
                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label15" runat="server" Text="联系人"></asp:Label></label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label16" runat="server" Text="手机号码"></asp:Label></label>
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label17" runat="server" Text="办公电话"></asp:Label></label>
                    <asp:TextBox ID="txtTEL" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label18" runat="server" Text="传真"></asp:Label></label>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label19" runat="server" Text="Email"></asp:Label></label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label20" runat="server" Text="QQ"></asp:Label></label>
                    <asp:TextBox ID="txtQQ" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label21" runat="server" Text="微信"></asp:Label></label>
                    <asp:TextBox ID="txtWechat" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label22" runat="server" Text="旺旺"></asp:Label></label>
                    <asp:TextBox ID="txtWangwang" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label23" runat="server" Text="国家"></asp:Label></label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label24" runat="server" Text="省"></asp:Label></label>
                    <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label25" runat="server" Text="市"></asp:Label></label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label26" runat="server" Text="区"></asp:Label></label>
                    <asp:TextBox ID="txtCounty" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label27" runat="server" Text="地址"></asp:Label></label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label28" runat="server" Text="邮编"></asp:Label></label>
                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row hidden">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label31" runat="server" Text="到期日期"></asp:Label></label>
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label32" runat="server" Text="提醒日期"></asp:Label></label>
                    <asp:TextBox ID="txtAlertDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label33" runat="server" Text="提醒内容"></asp:Label></label>
                    <asp:TextBox ID="txtAlertInfo" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label34" runat="server" Text="是否提醒"></asp:Label></label>
                    <br />
                    <asp:CheckBox ID="cbAlert" runat="server"></asp:CheckBox>
                  </div>
                  
              </div>

              <div class="row">
                    
                  <div class="form-group" style="padding:15px">
                      <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                  </div>
                        
              </div>
                    
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>